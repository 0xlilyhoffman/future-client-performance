
TODO
- setup project structure
    - infra/dataprovider/fururedata (api into future sandbox data, only including things we care about)
    - model = entites = structs/interfaces "things we care about"
    - restapi = GET /insights//user/:from/:to
    - services = aggregate data fetched from futuredata into meaningful entity

INFRA
- fetch data from future API (define endpoints/api, define payloads)
- define REST API (GET /insights/:user/:from/:to, response struct)

SERVICES
- define service interfaces 
    - interface to data provider, implemented by futuredata
    - interface to restapi, implemented by insightservice
- define service
    - insights service (fetches relevant data from db, presents to user)

MODEL
- any time you need a data entity used by multiple layers... add it here

1:36pm
----------------------------------

TODO
    - fetch minimal data from dataprovider
    - define model.User/Workout/WorkoutSummary, model.ClientInsight{}
    - aggregate raw data into client insights