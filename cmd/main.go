package main

import (
	"fmt"
	"future-client-performance/internal/infra/dataprovider/futuredata"
	"future-client-performance/internal/infra/entrypoints/restapi"
	"future-client-performance/internal/services/insightsservice"
	"log"
	"os"
	"os/signal"
	"syscall"
)

const (
	timeFormat   = "2006-01-02T15:04:05-07:00"
	serverListen = 8080
)

func main() {
	dataprovider := futuredata.NewSandboxDataProvider()
	insightsService := insightsservice.NewClientInsightsService(dataprovider)
	insightsAPI := restapi.NewInsightsService(insightsService, timeFormat)

	log.Printf("Listening on %d...\n", serverListen)

	serverError := make(chan error)
	signals := make(chan os.Signal, 10)
	signal.Notify(signals, os.Interrupt, syscall.SIGHUP)

	go func() {
		serverError <- insightsAPI.ListenAndServe(fmt.Sprintf(":%d", serverListen))
	}()

	// Wait for server error or request to terminate
	select {
	case signal := <-signals:
		log.Println("server received signal: ", signal)

	case err := <-serverError:
		log.Println("server exited with error: ", err)
	}

	log.Println("Exiting")
}
