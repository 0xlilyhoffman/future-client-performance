package restapi

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"future-client-performance/internal/model"
	"future-client-performance/internal/services/insightsservice"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/render"
)

const (
	URLParamUserID    = "user_id"
	URLParamStartTime = "starts_at"
	URLParamEndTime   = "ends_at"
)

type InsightsService struct {
	router          *chi.Mux
	insightsService insightsservice.InsightsService
	timeFormat      string
}

func NewInsightsService(insightsService insightsservice.InsightsService, timeFormat string) InsightsService {
	service := InsightsService{
		router:          chi.NewRouter(),
		insightsService: insightsService,
		timeFormat:      timeFormat,
	}
	service.setupRoutes()
	return service
}

func (s InsightsService) setupRoutes() {
	s.router.Use(
		render.SetContentType(render.ContentTypeJSON),
		middleware.Logger,
		middleware.Recoverer,
	)
	s.router.Get("/health", s.HealthCheck)
	s.router.Route(fmt.Sprintf("/insights/{%s}/{%s}/{%s}", URLParamUserID, URLParamStartTime, URLParamEndTime), func(r chi.Router) {
		r.Get("/", s.GetInsights)
	})
}

func (s InsightsService) ListenAndServe(address string) error {
	return http.ListenAndServe(address, s.router)
}

func (s InsightsService) HealthCheck(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "text/plain")
	w.Write([]byte("Healthy"))
}

type GetInsightsResponse struct {
	Insights model.ClientInsights `json:"insights"`
}

func (s InsightsService) GetInsights(w http.ResponseWriter, r *http.Request) {
	var (
		userIDParam    = chi.URLParam(r, URLParamUserID)
		startTimeParam = chi.URLParam(r, URLParamStartTime)
		endsTimeParam  = chi.URLParam(r, URLParamEndTime)
		resp           GetInsightsResponse
	)
	if userIDParam == "" || startTimeParam == "" || endsTimeParam == "" {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	startTime, err := time.Parse(s.timeFormat, startTimeParam)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	endTime, err := time.Parse(s.timeFormat, endsTimeParam)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	insights, err := s.insightsService.GetInsights(userIDParam, startTime, endTime)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	resp = GetInsightsResponse{
		Insights: insights,
	}

	WriteJSON(w, http.StatusOK, &resp)
}

func WriteJSON(w http.ResponseWriter, statusCode int, resp interface{}) {
	data, err := json.Marshal(&resp)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(statusCode)
	w.Header().Set("Content-Type", "application/json")
	w.Write(data)
}
