package futuredata

import (
	"fmt"
	"future-client-performance/internal/model"
	"future-client-performance/internal/services/insightsservice"
)

var _ insightsservice.InsightsStore = (SandboxDataProvider)(SandboxDataProvider{})

type SandboxDataProvider struct {
}

func NewSandboxDataProvider() SandboxDataProvider {
	return SandboxDataProvider{}
}

// Users

type UsersResponse []UserResponse

type UserResponse struct {
	ID        string `json:"id"`
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
}

func (r UserResponse) User() model.User {
	return model.User{
		ID:        r.ID,
		FirstName: r.FirstName,
		LastName:  r.LastName,
	}
}

func (c SandboxDataProvider) GetUsers() ([]model.User, error) {
	var (
		url           = "https://sandbox.future.fit/users"
		usersResponse = UsersResponse{}
		users         = []model.User{}
	)

	err := Get(url, &usersResponse)
	if err != nil {
		return users, err
	}

	for _, user := range usersResponse {
		users = append(users, user.User())
	}

	return users, nil
}

// Workouts

type WorkoutsResponse []WorkoutResponse
type WorkoutResponse struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

func (r WorkoutResponse) Workout() model.Workout {
	return model.Workout{
		ID:   r.ID,
		Name: r.Name,
	}
}

func (s SandboxDataProvider) GetWorkouts(userID string) ([]model.Workout, error) {
	var (
		url              = fmt.Sprintf("https://sandbox.future.fit/users/%s/workouts", userID)
		workoutsResponse = WorkoutsResponse{}
		workouts         = []model.Workout{}
	)
	err := Get(url, &workoutsResponse)
	if err != nil {
		return workouts, err
	}

	for _, workout := range workoutsResponse {
		workouts = append(workouts, workout.Workout())
	}

	return workouts, nil
}

// Summaries

type WorkoutSummariesResponse []WorkoutSummaryResponse

type WorkoutSummaryResponse struct {
	ID              string  `json:"id"`
	Difficulty      float32 `json:"difficulty"`
	CompletionState string  `json:"completion_state"`
}

func (r WorkoutSummaryResponse) WorkoutSummary() model.WorkoutSummary {
	return model.WorkoutSummary{
		ID:         r.ID,
		Difficulty: r.Difficulty,
		Completed:  r.CompletionState == "full",
	}
}

func (s SandboxDataProvider) GetWorkoutSummaries(workoutID string) ([]model.WorkoutSummary, error) {
	var (
		url                      = fmt.Sprintf("https://sandbox.future.fit/workouts/%s/summaries", workoutID)
		workoutSummariesResponse = WorkoutSummariesResponse{}
		workoutSummaries         = []model.WorkoutSummary{}
	)
	err := Get(url, &workoutSummariesResponse)
	if err != nil {
		return workoutSummaries, err
	}

	for _, workoutSummary := range workoutSummariesResponse {
		workoutSummaries = append(workoutSummaries, workoutSummary.WorkoutSummary())
	}

	return workoutSummaries, nil
}
