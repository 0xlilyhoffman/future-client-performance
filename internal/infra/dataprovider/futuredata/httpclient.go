package futuredata

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
)

// type FutureClient struct {
// 	// TODO: baseURL, Get(path)
// }

// func NewClient() FutureClient {
// 	return FutureClient{}
// }

// func (c FutureClient) Get(url string, out interface{}) error {
// 	response, err := http.Get(url)
// 	if err != nil {
// 		return err
// 	}

// 	body, err := ioutil.ReadAll(response.Body)
// 	if err != nil {
// 		return err
// 	}

// 	if response.StatusCode != http.StatusOK {
// 		return errors.New("unexpected status code")
// 	}

// 	err = json.Unmarshal(body, &out)
// 	if err != nil {
// 		return err
// 	}

// 	return nil

// }

func Get(url string, out interface{}) error {
	response, err := http.Get(url)
	if err != nil {
		return err
	}

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return err
	}

	if response.StatusCode != http.StatusOK {
		return errors.New("unexpected status code")
	}

	err = json.Unmarshal(body, &out)
	if err != nil {
		return err
	}

	return nil

}
