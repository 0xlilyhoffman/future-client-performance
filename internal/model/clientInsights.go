package model

type ClientInsights struct {
	// User      User
	// Workouts  []Workout
	// Summaries []WorkoutSummary
	// CONCLUSION THINGS....

	AveragDifficulty            float64
	PercentageCompletedWorkouts float64
}
