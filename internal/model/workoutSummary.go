package model

type WorkoutSummary struct {
	ID         string
	Difficulty float32
	Completed  bool
}
