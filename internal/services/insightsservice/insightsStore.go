package insightsservice

import "future-client-performance/internal/model"

type InsightsStore interface {
	GetUsers() ([]model.User, error)
	GetWorkouts(userID string) ([]model.Workout, error)
	GetWorkoutSummaries(workoutID string) ([]model.WorkoutSummary, error)
}
