package insightsservice

import (
	"future-client-performance/internal/model"
	"time"
)

var _ InsightsService = (ClientInsightsService)(ClientInsightsService{})

type ClientInsightsService struct {
	dataprovider InsightsStore
}

func NewClientInsightsService(dataprovider InsightsStore) ClientInsightsService {
	return ClientInsightsService{
		dataprovider: dataprovider,
	}
}

func (c ClientInsightsService) GetInsights(userID string, fromTime time.Time, toTime time.Time) (model.ClientInsights, error) {
	var (
		insights = model.ClientInsights{}
	)
	userWorkouts, err := c.dataprovider.GetWorkouts(userID)
	if err != nil {
		return insights, err
	}

	workoutSummaries, err := c.dataprovider.GetWorkoutSummaries(userWorkouts[0].ID) // TODO: bounds check
	if err != nil {
		return insights, err
	}

	// Aggregate metrics to generate insights
	var totalDifficulty = 0.0
	var completedCount = 0
	for _, summary := range workoutSummaries {
		totalDifficulty += float64(summary.Difficulty)
		if summary.Completed {
			completedCount++
		}
	}
	averageDifficulty := totalDifficulty / float64(len(workoutSummaries))
	averageCompletion := float64(completedCount) / float64(len(workoutSummaries))

	insights = model.ClientInsights{
		AveragDifficulty:            averageDifficulty,
		PercentageCompletedWorkouts: averageCompletion,
	}

	return insights, nil
}
