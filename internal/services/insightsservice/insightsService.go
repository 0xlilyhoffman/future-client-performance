package insightsservice

import (
	"future-client-performance/internal/model"
	"time"
)

type InsightsService interface {
	GetInsights(userID string, fromTime time.Time, toTime time.Time) (model.ClientInsights, error)
}
